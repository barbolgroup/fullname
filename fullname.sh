#!/bin/bash

#[by fleiva]
#DIRBASE=/home/barbol/fullname
DIRBASE=~/fullname
NC='\033[0m' 		# No Color
BBlue='\033[1;34m'  	# Blue
UPurple='\033[4;35m'	# Purple

function limpiarArchivo () { #Recibe el nombre de archivo que tiene que procesar
        echo "" > $DIRBASE"/nombreLimpio.txt"
        while IFS= read -r line
        do
                line="${line#"${line%%[![:space:]]*}"}"   # elimina los espacios por delante
                line="${line%"${line##*[![:space:]]}"}"   # elimina los espacios por detrás
                echo $line >>$DIRBASE"/nombreLimpio.txt"
        done < $1
        sed -i '/^ *$/d' $DIRBASE"/nombreLimpio.txt" #elimina filas en blanco
}

function borrarArchivos () {
	rm -rf $DIRBASE/curl.txt
	rm -rf $DIRBASE/filaSucia.txt
	rm -rf $DIRBASE/parteFilaSucia.txt
	rm -rf $DIRBASE/partecitaFilaSucia.txt
	rm -rf $DIRBASE/nombreEspacio.txt
	rm -rf $DIRBASE/nombreLimpio.txt
}

function ConsultarNosis () {
    texto="$1"
    endpoint="https://informes.nosis.com/Home/Buscar"
    body="Texto=$texto"

    # Realizar la solicitud HTTP POST
    jsonResponse=$(curl -s -X POST -d "$body" "$endpoint")

    # Verificar la respuesta JSON
    entidadesEncontradas=$(echo "$jsonResponse" | jq '.EntidadesEncontradas')

    count=$(echo "$entidadesEncontradas" | jq 'length')
    
    # Verificar si se encontraron entidades
    if [[ $count -gt 0 ]]; then
        for ((i = 0; i < $count; i++)); do
            documento=$(echo "$entidadesEncontradas" | jq -r ".[$i].Documento")
            razonSocial=$(echo "$entidadesEncontradas" | jq -r ".[$i].RazonSocial")
            provincia=$(echo "$entidadesEncontradas" | jq -r ".[$i].Provincia")
	    echo "============ NOSIS ============" | lolcat -a -s 20
	    echo -e "${BBlue}CUIL:$NC $documento"
            echo -e "${BBlue}NOMBRE:$NC $razonSocial"
            echo -e "${BBlue}PROVINCIA:$NC $provincia"
	    echo "===============================" | lolcat
        done
    else
	echo "================== NOSIS ==================" | lolcat -a -s 20   
        echo "🔴 No se encontraron datos en nosis.com 🔴"
	echo "===========================================" | lolcat
    fi
}

#INICIO PROGRAMA
cd $DIRBASE

ConsultarNosis $1

curl -s https://www.cuitonline.com/search.php\?q\=$1 | grep -a breadcrumbsContainer | cut -d'<' -f 10 | cut -d'>' -f 2 > $DIRBASE"/nombreLimpio.txt"
cantPalabras=`wc -w $DIRBASE"/nombreLimpio.txt" | awk '{print $1}'`;
        if test $cantPalabras -eq 0;
        then
		echo "================== CUITONLINE ==================" | lolcat -a -s 20	
                echo "🔴 No se encontraron datos en cuitonline.com 🔴" 
		echo "================================================" | lolcat
        else
		echo ""
		echo "============ CUITONLINE ============" | lolcat -a -s 20	
		resultado=`cat $DIRBASE"/nombreLimpio.txt"`
		echo "🟢 $resultado 🟢"
		echo "====================================" | lolcat	
        fi


borrarArchivos
